package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(Integer id, String start, String end) {
        final int LIMIT_NUM = 1000;
        Calendar calendar = Calendar.getInstance();

        String startDate;
        String endDate;

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isBlank(start)) {
           	 startDate = start + " 00:00:00";
           } else {
           	 startDate = "2020/01/01 00:00:00";
           }
           if(!StringUtils.isBlank(end)) {
           	 endDate = end + " 23:59:59";
           } else {
           	 endDate = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(calendar.getTime());
           }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, startDate, endDate);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void delete(String id) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, id);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    public void update(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    public Message editSelect(String id) {

        Connection connection = null;
        try {

            connection = getConnection();
            Message editMessage = new MessageDao().select(connection, id);
            commit(connection);

            return editMessage;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
